// -*- tab-width: 4; indent-tabs-mode: nil -*-

#ifndef DUNE_PDELAB_SYSTEMTESTING_HELMHOLTZ_BCEXTENSION_HH
#define DUNE_PDELAB_SYSTEMTESTING_HELMHOLTZ_BCEXTENSION_HH

/** \brief A function that defines Dirichlet boundary conditions AND
    its extension to the interior */
template<typename T>
class BCExtension
  : public Dune::PDELab::GridFunctionBase<Dune::PDELab::GridFunctionTraits<typename T::Traits::GridViewType,
									   typename T::Traits::RangeFieldType,
									   1,
									   Dune::FieldVector<typename T::Traits::RangeFieldType,1> >,
					  BCExtension<T> > {
public:
 typedef Dune::PDELab::GridFunctionTraits<typename T::Traits::GridViewType,
					  typename T::Traits::RangeFieldType,
					  1,
					  Dune::FieldVector<typename T::Traits::RangeFieldType,1> > Traits;

  //! construct from grid view
  BCExtension  (const typename Traits::GridViewType& gv_, T& t_) : gv(gv_), t(t_) {}

  //! evaluate extended function on element
  inline void evaluate (const typename Traits::ElementType& e,
                        const typename Traits::DomainType& xlocal,
                        typename Traits::RangeType& y) const
  {
    y = t.g(e, xlocal);
  }

  //! get a reference to the grid view
  inline const typename Traits::GridViewType& getGridView () const
  {
    return gv;
  }

private:
  const typename Traits::GridViewType gv;
  T& t;


};

#endif
