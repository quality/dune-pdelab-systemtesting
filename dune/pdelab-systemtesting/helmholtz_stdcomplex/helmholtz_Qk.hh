// -*- tab-width: 4; indent-tabs-mode: nil -*-


#ifndef DUNE_PDELAB_SYSTEMTESTING_HELMHOLTZ_QK_HH
#define DUNE_PDELAB_SYSTEMTESTING_HELMHOLTZ_QK_HH



/** \file

    \brief Construct GFS and solve problem defined in parameter
*/

template<int k, class GV, class PARAM>
void helmholtz_Qk (const GV& gv, PARAM& param, std::string& errornorm, std::string solver)
{
  // <<<1>>> Choose domain and range field type
  typedef typename GV::Grid::ctype Coord;
      typedef double R;
      typedef std::complex<R> C;
      typedef C RF;



  // <<<2>>> Make grid function space
  typedef Dune::PDELab::QkLocalFiniteElementMap<GV,Coord,C,k> FEM;
  //typedef Dune::PDELab::PkLocalFiniteElementMap<GV,Coord,C,k> FEM;
  FEM fem(gv);
  // typedef Dune::PDELab::NoConstraints CON;  // !!!!! NO CONTRAINTS
  typedef Dune::PDELab::ConformingDirichletConstraints CON; // constraints class
  typedef Dune::PDELab::ISTLVectorBackend<> VBE;
  typedef Dune::PDELab::GridFunctionSpace<GV,FEM,CON,VBE> GFS;
  GFS gfs(gv,fem);
  gfs.name("solution");
  //BCTypeParam bctype; // boundary condition type

  typedef typename GFS::template ConstraintsContainer<C>::Type CC;
  CC cc;
  Dune::PDELab::constraints( param, gfs, cc ); // assemble constraints
  gfs.update();
  std::cout << "constrained dofs=" << cc.size() << " of " << gfs.globalSize() << std::endl;


  // <<<3>>> Make grid operator
  typedef HelmholtzLocalOperator<PARAM > LOP;

  LOP lop( param, 2*k );
  typedef Dune::PDELab::istl::BCRSMatrixBackend<> MBE;
  // Structured 2D grid, Q1 finite elements => 9-point stencil / Q2 => 25
  MBE mbe(k == 1 ? 9 : 25);

  typedef Dune::PDELab::GridOperator<GFS,GFS,LOP,MBE,C,C,C,CC,CC> GO;
  //GO go(gfs,gfs,lop,mbe);
  GO go(gfs,cc,gfs,cc,lop,mbe);

  // How well did we estimate the number of entries per matrix row?
  // => print Jacobian pattern statistics
  //typename GO::Traits::Jacobian jac(go);
  //std::cout << jac.patternStatistics() << std::endl;



  typedef typename GO::Traits::Domain U;
  C zero(0.);
  U u(gfs,zero);                                      // initial value
  //ure.base().bar(); //determine type of u
  typedef BCExtension<PARAM > G;                      // boundary value = extension
  G g(gv,param);
  Dune::PDELab::interpolate(g,gfs,u);                // interpolate coefficient vector
  // Dune::PDELab::interpolate(param,gfs,u);
  Dune::PDELab::set_nonconstrained_dofs(cc,0.0,u);





  // <<<4>>> Select a linear solver backend
  // typedef Dune::PDELab::ISTLBackend_SEQ_BCGS_SSOR LS;
  //typedef Dune::PDELab::ISTLBackend_SEQ_SuperLU LS ;
  //typedef Dune::PDELab::ISTLBackend_SEQ_CG_SSOR LS;
  // typedef Dune::PDELab::ISTLBackend_SEQ_UMFPack LS;
  // LS ls(5000,0);
  // typedef Dune::PDELab::StationaryLinearProblemSolver<GO,LS,U> SLP;
  // SLP slp(go,ls,u,1e-10);
  // slp.apply();

  //-------------------------------------------------------
  // we do not use the backend, we call solver directly instead


  typedef typename Dune::PDELab::BackendVectorSelector<GFS,RF>::Type V;
  typedef typename GO::Jacobian M;

  typedef typename M::BaseT ISTLM;
  typedef typename V::BaseT ISTLV;


  M m(go,0.);
  //std::cout << m.patternStatistics() << std::endl;
  go.jacobian(u,m);
  V r(gfs);
  r = 0.0;
  go.residual(u,r);





    if(solver == "UMFPACK") {
      Dune::UMFPack<ISTLM> solver(m.base(), 0);
      r *= -1.0; // need -residual
      //u = r;
      // u = 0;
      Dune::InverseOperatorResult stat;
      solver.apply(u.base(),r.base(),stat);
    }
    if(solver == "SuperLU") {
      Dune::SuperLU<ISTLM> solver(m.base(), 0);
      r *= -1.0; // need -residual
      //u = r;
      // u = 0;
      Dune::InverseOperatorResult stat;
      solver.apply(u.base(),r.base(),stat);
    }



    Dune::InverseOperatorResult stat;

    if (solver == "GMRESILU0") {
      Dune::SeqILU0<ISTLM,ISTLV,ISTLV> ilu0(m.base(),1.0);
      Dune::MatrixAdapter<ISTLM,ISTLV,ISTLV> opa(m.base());
      Dune::RestartedGMResSolver<ISTLV> solver(opa, ilu0, 1E-7, 5000, 5000, 0);
      r *= -1.0; // need -residual
      //u = r;
      // u = 0;
      solver.apply(u.base(),r.base(),stat);
      std::cout<<"Iterations: "<< stat.iterations<<std::endl;
      std::cout<<"Time: "<<  stat.elapsed<< std::endl;
    }
    if (solver == "GMRESILU1") {
      Dune::SeqILUn<ISTLM,ISTLV,ISTLV> ilun(m.base(), 1, 1.0);
      Dune::MatrixAdapter<ISTLM,ISTLV,ISTLV> opa(m.base());
      Dune::RestartedGMResSolver<ISTLV> solver(opa, ilun, 1E-7, 5000, 5000, 0);
      r *= -1.0; // need -residual
      //u = r;
      // u = 0;
      solver.apply(u.base(),r.base(),stat);
      std::cout<<"Iterations: "<< stat.iterations<<std::endl;
      std::cout<<"Time: "<<  stat.elapsed<< std::endl;
    }
    if (solver == "GMRESSGS") {
      Dune::SeqSSOR<ISTLM,ISTLV,ISTLV> ssor(m.base(), 3, 1.); //ssor with \omega = 1 is SGS (symmetric gauss seidel)
      Dune::MatrixAdapter<ISTLM,ISTLV,ISTLV> opa(m.base());
      Dune::RestartedGMResSolver<ISTLV> solver(opa, ssor, 1E-7, 5000, 5000, 0);
      r *= -1.0; // need -residual
      //u = r;
      // u = 0;
      solver.apply(u.base(),r.base(),stat);
      std::cout<<"Iterations: "<< stat.iterations<<std::endl;
      std::cout<<"Time: "<<  stat.elapsed<< std::endl;
    }
    if (solver == "BiCGSILU0") {
      Dune::SeqILU0<ISTLM,ISTLV,ISTLV> ilu0(m.base(),1.0);
      Dune::MatrixAdapter<ISTLM,ISTLV,ISTLV> opa(m.base());
      Dune::BiCGSTABSolver<ISTLV> solver(opa,ilu0,1E-7,20000, 0);
      // solve the jacobian system
      r *= -1.0; // need -residual
      //u = r;
      // u = 0;
      solver.apply(u.base(),r.base(),stat);
      std::cout<<"Iterations: "<< stat.iterations<<std::endl;
      std::cout<<"Time: "<<  stat.elapsed<< std::endl;
    }

    //this solver breaks down at 4000 DOFs by solving the helmholtz eq
    if (solver == "BiCGSILU1") {
      Dune::SeqILUn<ISTLM,ISTLV,ISTLV> ilun(m.base(), 1, 1.0);
      Dune::MatrixAdapter<ISTLM,ISTLV,ISTLV> opa(m.base());
      Dune::BiCGSTABSolver<ISTLV> solver(opa,ilun,1E-7,20000, 0);
      // solve the jacobian system
      r *= -1.0; // need -residual
      // //u = r;
      // u = 0;
      solver.apply(u.base(),r.base(),stat);
      std::cout<<"Iterations: "<< stat.iterations<<std::endl;
      std::cout<<"Time: "<<  stat.elapsed<< std::endl;
    }
    if (solver == "BiCGSSGS") {
      Dune::SeqSSOR<ISTLM,ISTLV,ISTLV> ssor(m.base(), 3, 1.); //ssor with \omega = 1 is SGS (symmetric gauss seidel)
      Dune::MatrixAdapter<ISTLM,ISTLV,ISTLV> opa(m.base());
      Dune::BiCGSTABSolver<ISTLV> solver(opa,ssor,1E-7,20000, 0);
      r *= -1.0; // need -residual
      // //u = r;
      // u = 0;
      solver.apply(u.base(),r.base(),stat);
      std::cout<<"Iterations: "<< stat.iterations<<std::endl;
      std::cout<<"Time: "<<  stat.elapsed<< std::endl;
    }




  // // // compare helmholtz solution to analytic helmholtz solution
  U ua(gfs, zero);                                      // initial value
  typedef BCAnalytic<PARAM> Ga;                      // boundary value = extension
  Ga ga(gv, param);
  Dune::PDELab::interpolate(ga,gfs,ua);                // interpolate coefficient vector


  // // Make a real grid function space
  typedef Dune::PDELab::QkLocalFiniteElementMap<GV,Coord,R,k> FEMr;
  typedef Dune::PDELab::GridFunctionSpace<GV,FEMr,CON,VBE> GFSr;
  FEMr femr(gv);
  GFSr gfsr(gv,femr);

  //create real analytic solution vectors
  typename Dune::PDELab::BackendVectorSelector<GFSr,R>::Type reu(gfsr, 0.);  // real part u_h
  typename Dune::PDELab::BackendVectorSelector<GFSr,R>::Type imu(gfsr, 0.);  // imag part u_h
  typename Dune::PDELab::BackendVectorSelector<GFSr,R>::Type reua(gfsr, 0.); // real part analytic solution
  typename Dune::PDELab::BackendVectorSelector<GFSr,R>::Type imua(gfsr, 0.); // imega part analytic solution
  // typename Dune::PDELab::BackendVectorSelector<GFSr,R>::Type reue(gfsr, 0.); // real part error
  // typename Dune::PDELab::BackendVectorSelector<GFSr,R>::Type imue(gfsr, 0.); // imag part error

  typename Dune::PDELab::BackendVectorSelector<GFSr,R>::Type::iterator reut = reu.begin();
  typename Dune::PDELab::BackendVectorSelector<GFSr,R>::Type::iterator imut = imu.begin();
  typename Dune::PDELab::BackendVectorSelector<GFSr,R>::Type::iterator reuat = reua.begin();
  typename Dune::PDELab::BackendVectorSelector<GFSr,R>::Type::iterator imuat = imua.begin();
  // typename Dune::PDELab::BackendVectorSelector<GFSr,R>::Type::iterator reuet = reue.begin();
  // typename Dune::PDELab::BackendVectorSelector<GFSr,R>::Type::iterator imuet = imue.begin();


  typename U::const_iterator ut = u.begin();
  typename U::const_iterator uat = ua.begin();

  // //compute error i.e. |ua - u| separated into real and imag part
  while( ut != u.end() ) {

    *reut = std::real(*ut);
    *imut = std::imag(*ut);
    *reuat = std::real(*uat);
    *imuat = std::imag(*uat);
    // *reuet = std::abs(*reut - *reuat) ;
    // *imuet = std::abs(*imut - *imuat) ;

    // std::cout<<h<<"\t"<<*reut - (*reuat)<<std::endl;
    //   ++h;

    ++ut;
    ++uat;

    ++reut;
    ++imut;
    ++reuat;
    ++imuat;
    // ++reuet;
    // ++imuet;

  }


  //<<<7>>> graphical output

  // output u_h
  //Dune::SubsamplingVTKWriter<GV> vtkwriter(gv,k == 1 ? 0 : 3);
  Dune::SubsamplingVTKWriter<GV> vtkwriter(gv, 0);


  gfsr.name("real");
  Dune::PDELab::addSolutionToVTKWriter(vtkwriter,gfsr,reu);

  gfsr.name("imaginary");
  Dune::PDELab::addSolutionToVTKWriter(vtkwriter,gfsr,imu);

  gfsr.name("real_analytic");
  Dune::PDELab::addSolutionToVTKWriter(vtkwriter,gfsr,reua);
  gfsr.name("imaginary_analytic");
  Dune::PDELab::addSolutionToVTKWriter(vtkwriter,gfsr,imua);


  // gfsr.name("real_error");
  // Dune::PDELab::addSolutionToVTKWriter(vtkwriter,gfsr,reue);
  // gfsr.name("imaginary_error");
  // Dune::PDELab::addSolutionToVTKWriter(vtkwriter,gfsr,imue);


  std::stringstream basename;
  basename << "solvers01_Q" << k;
  vtkwriter.write(basename.str(),Dune::VTK::appendedraw);






 // //  // compute  DISCRETIZATION L2 error
 // //  /* solution */
 //  if(errornorm == "L2") {
 //    // define discrete grid function for solution
 //    typedef Dune::PDELab::DiscreteGridFunction<GFS,U> DGF;
 //    // DGF udgf(gfs, u);

 //    typedef BCAnalytic<PARAM> ES;
 //    ES es(gv, param);
 //    U esh(gfs, zero);
 //    Dune::PDELab::interpolate(es,gfs,esh);
 //    DGF esdgf(gfs, esh);

 //    typedef DifferenceSquaredAdapter<DGF,ES> DifferenceSquared;
 //    DifferenceSquared difference(esdgf, es);

 //    typename DifferenceSquared::Traits::RangeType l2normsquared(0.0);
 //    Dune::PDELab::integrateGridFunction(difference,l2normsquared,10);

 //    std::cout<<"L2DiscrError: "<<std::sqrt(std::abs(l2normsquared[0]))<<std::endl;
 //  }



 // //  // compute  POLLUTION L2 error
 // //  /* solution */
 //  if(errornorm == "L2") {
 //    // define discrete grid function for solution
 //    typedef Dune::PDELab::DiscreteGridFunction<GFS,U> DGF;
 //    DGF udgf(gfs, u);

 //    typedef BCAnalytic<PARAM> ES;
 //    ES es(gv, param);
 //    U esh(gfs, zero);
 //    Dune::PDELab::interpolate(es,gfs,esh);
 //    DGF esdgf(gfs, esh);


 //    typedef DifferenceSquaredAdapter<DGF,DGF> DifferenceSquared;
 //    DifferenceSquared difference(udgf,esdgf);

 //    typename DifferenceSquared::Traits::RangeType l2normsquared(0.0);
 //    Dune::PDELab::integrateGridFunction(difference,l2normsquared,10);

 //    std::cout<<"L2PolError: "<<std::sqrt(std::abs(l2normsquared[0]))<<std::endl;

 //  }





 //  // compute L2 error
 //  /* solution */
  if(errornorm == "L2") {
    // define discrete grid function for solution
    typedef Dune::PDELab::DiscreteGridFunction<GFS,U> DGF;
    DGF udgf(gfs, u);

    typedef BCAnalytic<PARAM> ES;
    ES es(gv, param);

    typedef DifferenceSquaredAdapter<DGF,ES> DifferenceSquared;
    DifferenceSquared difference(udgf,es);

    typename DifferenceSquared::Traits::RangeType l2normsquared(0.0);
    Dune::PDELab::integrateGridFunction(difference,l2normsquared,10);

    std::cout<<"L2Error: "<<std::sqrt(std::abs(l2normsquared[0]))<<std::endl;
  }



  // compute H1 semi norm error
  /* solution */
  if(errornorm == "H1") {
    //discrete function gradient
    typedef Dune::PDELab::DiscreteGridFunctionGradient<GFS,U> DGFG;
    DGFG udgfg(gfs, u);

    //gradient of exact solution
    typedef BCAnalyticGrad<PARAM> ESG;
    ESG esg(gv, param);

    // difference of gradient
    typedef DifferenceSquaredAdapter<DGFG,ESG> DifferenceSquaredAdapterg;
    DifferenceSquaredAdapterg differenceg(udgfg,esg);

    typename DifferenceSquaredAdapterg::Traits::RangeType l2normsquared(0.0);
    typename DifferenceSquaredAdapter<DGFG,ESG>::Traits::RangeType normsquared(0.0);
    Dune::PDELab::integrateGridFunction(differenceg,normsquared,10);

    std::cout<<"H1Error: "<<std::sqrt(std::abs(l2normsquared[0]))<<std::endl;

  }




}

#endif
