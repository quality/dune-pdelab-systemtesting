// -*- tab-width: 4; indent-tabs-mode: nil -*-

#ifndef DUNE_PDELAB_SYSTEMTESTING_HELMHOLTZ_MAIN_HH
#define DUNE_PDELAB_SYSTEMTESTING_HELMHOLTZ_MAIN_HH

/** \file

    \brief main
*/

int main(int argc, char** argv)
{
  try{
    //Maybe initialize Mpi
    Dune::MPIHelper& helper = Dune::MPIHelper::instance(argc, argv);
    if(Dune::MPIHelper::isFake)
      std::cout<< "This is a sequential program." << std::endl;
    else
      {
        if(helper.rank()==0)
          std::cout << "parallel run on " << helper.size() << " process(es)" << std::endl;
      }


    if (argc!=2)
      {
        if (helper.rank()==0)
          std::cout << "usage: ./error <cfg-file>" << std::endl;
        return 1;
      }


    // Parse configuration file.
    std::string config_file(argv[1]);
    Dune::ParameterTree configuration;
    Dune::ParameterTreeParser parser;


    try{
      parser.readINITree( config_file, configuration );
    }
    catch(...){
      std::cerr << "Could not read config file \""
                << config_file << "\"!" << std::endl;
      exit(1);
    }

    int level = configuration.get<int>("grid.level");
    int polynomialdegree = configuration.get<int>("problem_parameter.polynomialdegree");
    double omega = configuration.get<double>("problem_parameter.omega") ;
    std::string errornorm = configuration.get<std::string>("norm.errornorm");
    std::string solver = configuration.get<std::string>("solvers.solver");




    // sequential version
    if (helper.size()==1)
      {

        std::vector<double> dof;
        std::vector<double> tsolve; // number of iterations + solver time


        typedef double R;
        typedef std::complex<R> C;

        Dune::FieldVector<double,2> L(1.);
        Dune::array<int,2> N(Dune::fill_array<int,2>(1));
        std::bitset<2> periodic(false);
        int overlap=0;
        Dune::YaspGrid<2> grid(L,N,periodic,overlap);
        typedef Dune::YaspGrid<2>::LeafGridView GV;

        //refine grid
        grid.globalRefine(level);
        //get leafGridView
        const GV& gv=grid.leafGridView();

        //define problem
        typedef ParametersPlaneWave<GV, C, R> PARAM;
        //typedef ParametersSphericalWave<GV, C, R> PARAM;

        PARAM param(omega);


        if(polynomialdegree == 1) {
          helmholtz_Qk<1,GV,PARAM>(gv, param,  errornorm, solver);
        }

        if(polynomialdegree == 2) {
          helmholtz_Qk<2,GV,PARAM>(gv, param, errornorm, solver);
        }




      }
  }
  catch (Dune::Exception &e){
    std::cerr << "Dune reported error: " << e << std::endl;
    return 1;
  }
  catch (...){
    std::cerr << "Unknown exception thrown!" << std::endl;
    return 1;
  }
}

#endif
